#ifndef NEWBOOKUNT_H
#define NEWBOOKUNT_H

#include <QDialog>
#include "MainUnt.h"

namespace Ui {
    class TNewBookFrm;
}

class TNewBookFrm : public QDialog
{
    Q_OBJECT

public:
    explicit TNewBookFrm(QWidget *parent = 0);
    ~TNewBookFrm();

private:
    Ui::TNewBookFrm *ui;
    BookInfo fBookInfo;
private:
    void SetNewCover( QString coverpath );
    void ShowCover();
private slots:
    void onBrowseCoverClicked();
public:
    void StoreBookInfo(const BookInfo& info);
    BookInfo GetBookInfo();
};

#endif // NEWBOOKUNT_H
