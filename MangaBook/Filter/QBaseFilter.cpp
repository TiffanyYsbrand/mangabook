#include <QImage>
#include <math.h>
#include "QBaseFilter.h"
#include <map>

template<class T>
inline const T& kClamp( const T& x, const T& low, const T& high )
{
    if ( x < low )       return low;
    else if ( high < x ) return high;
    else                 return x;
}

inline
int changeBrightness( int value, int brightness )
{
    return kClamp( value + brightness * 255 / 100, 0, 255 );
}

inline
int changeContrast( int value, int contrast )
{
    return kClamp((( value - 127 ) * contrast / 100 ) + 127, 0, 255 );
}

inline
int changeGamma( int value, int gamma )
{
    return kClamp( int( pow( value / 255.0, 100.0 / gamma ) * 255 ), 0, 255 );
}

inline
int changeUsingTable( int value, const int table[] )
{
    return table[ value ];
}

typedef struct tagGammaTable {
    int table[256];
} GammaArray;

std::map < int, GammaArray > LocalGammaTables;

template< int operation( int, int ) >
static
void changeImage( QImage& image, int value )
{
    image.detach();
    // TODO: Cache Generated table
    const int* table;
    std::map < int, GammaArray >::iterator it = LocalGammaTables.find(value);
    if (it == LocalGammaTables.end()) {
        GammaArray array;
        for( int i = 0; i < 256; ++i )
        {
            array.table[ i ] = operation( i, value );
        }
        LocalGammaTables.insert(std::make_pair(value, array));
        it = LocalGammaTables.find(value);
        table = it->second.table;
    } else {
        table = it->second.table;
    }
    //int table[256];
    //for( int i = 0; i < 256; ++i )
    //{
    //    table[ i ] = operation( i, value );
    //}
    if( image.numColors() == 0 ) // 8bit grayscale, or 16/24/32 bit image
    {
        if( image.format() != QImage::Format_RGB32 && image.format() != QImage::Format_ARGB32 ) /* just in case */
        {
            image = image.convertToFormat( QImage::Format_RGB32 );
        }
        if( image.hasAlphaChannel() ) {
            // we should never reach here because ARGB is converted to rgb
            for( int y = 0; y < image.height(); ++y ) {
                uchar* line = image.scanLine(y);
                const uchar* lineend = line + image.width() * 4;
                while (line < lineend) {
                    //*line = table[*line]; // B
                    //line++;
                    //*line = table[*line]; // G
                    //line++;
                    //*line = table[*line]; // R
                    //line++;
                    //*line = table[*line]; // A
                    //line++;
                    *line = table[*line]; // May slower?
                    line++;
                }
                //QRgb* line = reinterpret_cast< QRgb* >( image.scanLine( y ));
                //for( int x = 0; x < image.width(); ++x ) {
                //    line[ x ] = qRgba( changeUsingTable( qRed( line[ x ] ), table ),
                //    changeUsingTable( qGreen( line[ x ] ), table ),
                //    changeUsingTable( qBlue( line[ x ] ), table ),
                //    changeUsingTable( qAlpha( line[ x ] ), table ));
                //}
            }
        } else {
            // RGB32
            for( int y = 0; y < image.height(); ++y ) {
                // +0B +1G +2R +3A
                uchar* line = image.scanLine(y);
                const uchar* lineend = line + image.width() * 4;
                while (line < lineend) {
                    *line = table[*line];
                    line++;
                    *line = table[*line];
                    line++;
                    *line = table[*line];
                    line+=2;
                }
                //QRgb* line = reinterpret_cast< QRgb* >( image.scanLine( y ));
                //for( int x = 0; x < image.width(); ++x ) {
                //    line[ x ] = qRgb( changeUsingTable( qRed( line[ x ] ), table ),
                //    changeUsingTable( qGreen( line[ x ] ), table ),
                //    changeUsingTable( qBlue( line[ x ] ), table ));
                //}
            }
        }
    } else {
        // Indexed color
        QVector<QRgb> colors = image.colorTable();
        for( int i = 0; i < image.numColors(); ++i ) {
            colors[ i ] = qRgb( changeUsingTable( qRed( colors[ i ] ), table ),
                changeUsingTable( qGreen( colors[ i ] ), table ),
                changeUsingTable( qBlue( colors[ i ] ), table ));
            //colors[ i ] = qRgb( operation( qRed( colors[ i ] ), value ),
            //operation( qGreen( colors[ i ] ), value ),
            //operation( qBlue( colors[ i ] ), value ));
        }
    }
}


// brightness is multiplied by 100 in order to avoid floating point numbers
void changeBrightness( QImage& image, int brightness )
{
    if( brightness == 0 ) // no change
        return;
    changeImage< changeBrightness >( image, brightness );
}


// contrast is multiplied by 100 in order to avoid floating point numbers
void changeContrast( QImage& image, int contrast )
{
    if( contrast == 100 ) // no change
        return;
    changeImage< changeContrast >( image, contrast );
}

// gamma is multiplied by 100 in order to avoid floating point numbers
void changeGamma( QImage& image, int gamma )
{
    if( gamma == 100 ) // no change
        return;
    changeImage< changeGamma >( image, gamma );
}

void prepareGammaTable(int gamma)
{
    if ( LocalGammaTables.find(gamma) == LocalGammaTables.end()) {
        GammaArray array;
        for( int i = 0; i < 256; ++i )
        {
            array.table[ i ] = changeGamma( i, gamma );
        }
        LocalGammaTables.insert(std::make_pair(gamma, array));
    }
}
