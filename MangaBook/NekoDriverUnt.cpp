#include "NekoDriverUnt.h"
#include "Windows.h" // InterlockedExchange
#include "Reader/pkparser/pkparser.h"
#include "Filter/QResample.h"
#include "AddonFuncUnt.h"
#include "DbCentre.h"


PNekoDriver theNekoDriver;

TNekoDriver::TNekoDriver( TMainFrm *frm )
    : fDoublePageMode(mdpmSingle)
    , fGammaCorrect(true)
    , fSharpen(false)
    , fDitherMode(false)
    , fJpegMode(true)
    , fScaleFilter(sfLanczos3)
    , fStageWidth(540)
    , fStageHeight(700)
    , fCodecQuality(80)

{
    // we create from MainFrm.
    QObject::connect(&fBookDepository, SIGNAL(logStored(QString, TLogType)),
        frm, SLOT(writeLog(QString, TLogType)));

    fFolderIcon.addFile(":/MainFrm/PpsDES/16x16-gnome2/stock_folder.png");
    fFolderIconOpened.addFile(":/MainFrm/PpsDES/16x16-gnome2/folder_open.png");
    fImageIcon.addFile(":/MainFrm/PpsDES/16x16-tango/image.png"); // TODO: IconProvider, fIkonStd
    fDelIcon.addFile(":/MainFrm/PpsDES/16x16-tango/trashcan_full.png");
    fCloakingIcon.addFile(":/MainFrm/PpsDES/Cloaking.png");
}

bool TNekoDriver::IsProjectEmpty()
{
    return fChapters.isEmpty();
}

bool TNekoDriver::LoadProjectFromFile( QString bookfilename )
{
    bool Result = false;

    //Result = fXipHelper.LoadProjectFromFile(mpkfilename);
    QFile file(bookfilename);
    file.open(QFile::ReadOnly);
    // TODO: MIME check
    QDataStream reader(&file);

    quint32 magic;

    reader >> magic;

    if (magic == 'HN01' || magic == 'HNU1' || magic == 'HNB1') {
        if (magic == 'HN01') {
            reader >> fGammaCorrect >> fSharpen >> fDitherMode >> fJpegMode; // Settings (may conflict with global settings)
            reader >> fDoublePageMode >> fScaleFilter >> fStageWidth >> fStageHeight >> fCodecQuality; // Parsed or clone of global settings
            reader >> fBookRec >> fChapters;
        } else if (magic == 'HNU1') {
            // UCL Compression
        }
    } else {
        // Error
    }

    file.close();
    Result = true;

    return Result;
}

bool TNekoDriver::SaveProjectToFile( QString bookfilename, bool compress )
{
    bool Result = false;

    QFile originalfile(bookfilename);
    bool keepbackup = originalfile.exists();
    if (keepbackup) {
        originalfile.rename(bookfilename + ".bak");
    }

    QFile file(bookfilename);
    file.open(QFile::WriteOnly);
    // TODO: UCL/BZ2 Compression
    if (compress) {

        file.close();
        Result = true;
    } else {
        QDataStream writer(&file);
        quint32 magic = 'HN01';
        writer << magic;
        writer << fGammaCorrect << fSharpen << fDitherMode << fJpegMode; // Settings (may conflict with global settings)
        writer << fDoublePageMode << fScaleFilter << fStageWidth << fStageHeight << fCodecQuality; // Parsed or clone of global settings
        writer << fBookRec << fChapters;
        file.close();
        Result = true;
    }

    if (keepbackup && Result) {
        originalfile.remove();
    }

    if (Result) {
        //fXipHelper.UpdateProjectModifiedStatus(false);
    }

    return Result;
}

bool TNekoDriver::KeepProjectView( QTreeWidget *treeview )
{
    treeview->clear();
    int chapterindex = 0;
    foreach(TChapterBundleRec chapter, fChapters) {
        uint count = chapter.Pages.size();
        QStringList cols;
        cols << chapter.Chaptername << QString("%1").arg(count);
        QTreeWidgetItem *chaptertree = new QTreeWidgetItem(cols);
        chaptertree->setIcon(0, fFolderIcon);
        chaptertree->setData(0, Qt::UserRole, 0); // 0 = Chapter, 1 = Page
        chaptertree->setData(0, Qt::UserRole + 1, chapterindex);
        chaptertree->setFlags(chaptertree->flags() | Qt::ItemIsEditable);

        int pageindex = 0;
        foreach(TPageBundleRec page, chapter.Pages) {
            QStringList newcols;
            newcols << page.Semipath << QString("%1").arg(pageindex + 1) << page.Fullpath;
            QTreeWidgetItem *pagenode = new QTreeWidgetItem((QTreeWidgetItem*)0, newcols);
            pagenode->setIcon(0, fImageIcon);
            pagenode->setData(0, Qt::UserRole, 1); // 0 = Chapter, 1 = Page
            pagenode->setData(0, Qt::UserRole + 1, chapterindex);
            pagenode->setData(0, Qt::UserRole + 2, pageindex);
            chaptertree->addChild(pagenode);
            pageindex++;
        }
        chapterindex++;

        //treeview->insertTopLevelItem(0, chaptertree);
        treeview->addTopLevelItem(chaptertree);
        //treeview->expandAll();
    }

    treeview->resizeColumnToContents(0);
    treeview->resizeColumnToContents(1);

    return true;
}

int TNekoDriver::GetAutoChapterIndex( QString folder )
{
    int result = -1;

    QString lowerkey = folder.toLower();
    QStringIntMap::iterator it = fDropedChapterLookupTable.find(lowerkey);
    if (it != fDropedChapterLookupTable.end()) {
        // rename?
        result = *it;
        return result;
    }
    return result;
}

int TNekoDriver::AddAutoChapter( QString folder, QString name )
{
    int result = -1;

    QString lowerkey = folder.toLower();
    QStringIntMap::iterator it = fDropedChapterLookupTable.find(lowerkey);
    if (it != fDropedChapterLookupTable.end()) {
        // rename?
        return result;
    }
    result = fChapters.size();
    fDropedChapterLookupTable.insert(lowerkey, result);
    TChapterBundleRec item;
    item.Chaptername = name;
    fChapters.append(item);

    return result;
}

int TNekoDriver::AddChapter( QString name )
{
    int result = fChapters.size();
    TChapterBundleRec item;
    item.Chaptername = name;
    fChapters.append(item);

    return result;
}

bool TNekoDriver::RenameChapter( int index, QString newname )
{
    bool result = false;
    if (index < 0 || index >= fChapters.size()) {
        return result;
    }
    if (fChapters[index].Chaptername.compare(newname, Qt::CaseSensitive) == 0) {
        return result;
    }
    fChapters[index].Chaptername = newname;
    result = true;
    return result;
}

QString TNekoDriver::GetChapterName( int index )
{
    QString result;
    if (index < 0 || index >= fChapters.size()) {
        return result;
    }
    result = fChapters[index].Chaptername;
    return result;
}

bool TNekoDriver::AddPageToChapter( int chapterindex, QString filename )
{
    bool result = false;
    if (chapterindex < 0 || chapterindex >= fChapters.size()) {
        return result;
    }
    // prescale
    TPageBundleRec item;
    item.Fullpath = filename;
    item.Folderpath = QFileInfo(filename).path();
    item.Semipath = QFileInfo(filename).fileName();
    item.FromArchive = false;
    
    //BuildSinglePrefetch(item, 540, 700);

    fChapters[chapterindex].Pages.append(item);
    result = true;
    return result;
}

bool TNekoDriver::AddArchivedPagesToChapter( int chapterindex, QString filename )
{
    bool result = false;
    if (chapterindex < 0 || chapterindex >= fChapters.size()) {
        return result;
    }

    PageBundleList list;
    BuildPKArchiveScrollList(filename, list);
    if (list.isEmpty()) {
        return result;
    }

    for (PageBundleList::iterator it = list.begin(); it != list.end(); /*it++*/) {
        //BuildSinglePrefetch(*it, 540, 700);
        if (it->StreamSize == 0) {
            it = list.erase(it);
        } else {
            it++;
        }
    }

    fChapters[chapterindex].Pages.append(list);
    result = true;
    return result;
}

bool TNekoDriver::RemovePage( int chapterindex, int pageindex )
{
    bool result = false;
    if (chapterindex < 0 || chapterindex >= fChapters.size()) {
        return result;
    }
    if (pageindex < 0 || pageindex >= fChapters[chapterindex].Pages.size()) {
        return result;
    }
    fChapters[chapterindex].Pages[pageindex].DropImageStore();
    fChapters[chapterindex].Pages.removeAt(pageindex);
    result = true;
    return result;
}

bool TNekoDriver::RemoveChapter( int index )
{
    bool result = false;
    if (index < 0 || index >= fChapters.size()) {
        return result;
    }
    foreach(TPageBundleRec page, fChapters[index].Pages) {
        page.DropImageStore();
    }
    fChapters.removeAt(index);
    result = true;
    return result;
}

bool TNekoDriver::CleanAutoChapterLookupTable()
{
    if (fDropedChapterLookupTable.isEmpty()) {
        return false;
    }
    fDropedChapterLookupTable.clear();
    return true;
}

bool TNekoDriver::ClearAllChapters()
{
    int result = false;
    if (fChapters.isEmpty()) {
        return result;
    }
    while (RemoveChapter(0)) {
        result = true;
    }
    return result;
}

bool ChapterCompareAsc(const TChapterBundleRec& rec1, const TChapterBundleRec& rec2) {
    return rec1.Chaptername.compare(rec2.Chaptername) < 0;
}

bool TNekoDriver::SortChapters(PageSortingMethod method)
{
    if (fChapters.isEmpty()) {
        return false;
    }
    qSort(fChapters.begin(), fChapters.end(), ChapterCompareAsc);
    if (method != psmNone) {
        for (int i = 0; i < fChapters.size(); i++) {
            SortPages(i, method);
        }
    }
    return true;
}

int S2Int( const ushort* S )
{
    int M;
    int Result = 0;
    if (S == NULL) {
        return Result;
    }
    M = 1;
    if (*S == L'-') {
        M = -1;
        S++;
    } else if (*S == L'+'){
        S++;
    }
    while (*S >= L'0' && *S <= L'9')
    {
        Result = Result * 10 + int( *S ) - int( L'0' );
        S++;
    }
    if (M < 0){
        Result = -Result;
    }
    return Result;
}

bool PageCompareNumericAsc(const TPageBundleRec& rec1, const TPageBundleRec& rec2) {
    int num1 = S2Int(QFileInfo(rec1.Semipath).baseName().utf16());
    int num2 = S2Int(QFileInfo(rec2.Semipath).baseName().utf16());
    if (num1 == num2) {
        return rec1.Semipath.compare(rec2.Semipath, Qt::CaseInsensitive) < 0;
    } else {
        return num1 < num2;
    }
}

bool PageCompareAlphaAsc(const TPageBundleRec& rec1, const TPageBundleRec& rec2) {
    return rec1.Semipath.compare(rec2.Semipath, Qt::CaseSensitive) < 0;
}

bool TNekoDriver::SortPages(int chapterindex, PageSortingMethod method) {
    if (method == psmNone) {
        return false;
    }
    if (chapterindex < 0 || chapterindex >= fChapters.size()) {
        return false;
    }
    TChapterBundleRec& chapter = fChapters[chapterindex];
    if (method == psmAutomatic) {
        // TODO: Check name to decide use numeric or case insensitive
        qSort(chapter.Pages.begin(), chapter.Pages.end(), PageCompareNumericAsc);
    }
    if (method == psmAlpha) {
        qSort(chapter.Pages.begin(), chapter.Pages.end(), PageCompareAlphaAsc);
    }
    return true;
}

bool TNekoDriver::SortAutoChapterPages(PageSortingMethod method) {
    if (fDropedChapterLookupTable.isEmpty() || method == psmNone) {
        return false;
    }
    foreach(int chapterindex, fDropedChapterLookupTable.values()) {
        SortPages(chapterindex, method);
    }
    return true;
}

void TNekoDriver::SetBookInfo( const QString& title, const QString& author, const QString& brief )
{
    fBookRec.BookTitle = title;
    fBookRec.Author = author;
    fBookRec.Brief = brief;
}

const QImage* TNekoDriver::GetCover()
{
    return fBookRec.Cover;
}

void TNekoDriver::SetCover( const QString& filename )
{
    if (filename.isEmpty()) {
        if (fBookRec.Cover) {
            delete fBookRec.Cover;
            fBookRec.Cover = NULL;
        }
        return;
    }

    fBookRec.Cover = new QImage(filename, MIMETypeToExt(GuessMIMEType(filename)).toAscii());
    ScaleCover(fBookRec.Cover);
}

void TNekoDriver::SetCover( const QImage* cover )
{
    if (cover == NULL || cover->isNull()) {
        // cover->isNull() only evaluate if cover pointer is valid
        // In fact delete a NULL pointer is safe
        delete fBookRec.Cover;
        fBookRec.Cover = NULL;
        return;
    }
    if (fBookRec.Cover == NULL) {
        fBookRec.Cover = new QImage(*cover);
    } else {
        *fBookRec.Cover = *cover;
    }
    ScaleCover(fBookRec.Cover);
}

void TNekoDriver::ScaleCover(QImage* cover)
{
    if (cover && (cover->width() != 120 || cover->height() != 150)) {
        QSize targetsize(cover->size());
        targetsize.scale(120, 150, Qt::KeepAspectRatio);
        *cover = CreateResampledBitmap(*cover, targetsize.width(), targetsize.height(), STOCK_FILTER_LANCZOS3);
        UncropImage(*cover, 120, 150); // Padding
    }
}

bool TNekoDriver::PrepareBufferStock( QLabel *label /*= 0*/, QProgressBar* bar /*= 0*/ )
{
    bool Result = false;
    if (label != NULL) {
        QObject::connect(this, SIGNAL(prepareBufferStageChanged(QString)),
            label, SLOT(setText(QString)), Qt::QueuedConnection);
    }
    if (bar != NULL) {
        QObject::connect(this, SIGNAL(prepareBufferProgressChanged(int)),
            bar, SLOT(setValue(int)), Qt::QueuedConnection);
    }

    Result = PreparePageImagesBuffer();// fXipHelper.PrepareFlatBufferFromProjectSemiStock();

    //if (label != NULL) {
    //    QObject::disconnect(this, SIGNAL(prepareBufferStageChanged(QString)),
    //        label, SLOT(setText(QString)));
    //}
    //if (bar != NULL) {
    //    QObject::disconnect(this, SIGNAL(prepareBufferProgressChanged(int)),
    //        bar, SLOT(setValue(int)));
    //}
    return Result;
}

bool TNekoDriver::BuildBookFromBuffer( QString snbfilename, QProgressBar* bar /*= 0*/ )
{
    bool Result = false;
    if (bar != NULL) {
        QObject::connect(&fBookDepository, SIGNAL(snbProgressChanged(int)),
            bar, SLOT(setValue(int)));
    }
    //fBookDepository.CleanupAllBuffers();
    Result = fBookDepository.GenerateBookFromBuffer(snbfilename);
    //if (bar != NULL) {
    //    QObject::disconnect(&fBookDepository, SIGNAL(snbProgressChanged(int)),
    //        bar, SLOT(setValue(int)));
    //}
    return Result;

}

bool TNekoDriver::MoveupChapters(QSet < int > indexes)
{
    QList < int > indexlist = indexes.toList();
    qSort(indexlist.begin(), indexlist.end(), qLess<int>());
    // 2, 3, 5, 6
    foreach(int index, indexlist) {
        if (index < 1) {
            return false;
        }
        fChapters.swap(index, index - 1);
    }
    return true;
}

bool TNekoDriver::MovedownChapters(QSet < int > indexes)
{
    QList < int > indexlist = indexes.toList();
    qSort(indexlist.begin(), indexlist.end(), qGreater<int>());
    // 6, 5, 3, 2
    foreach(int index, indexlist) {
        if (index >= fChapters.size() - 1) {
            return false;
        }
        fChapters.swap(index, index + 1);
    }
    return true;
}

//ScaleFilter TNekoDriver::filter()
//{
//    return fScaleFilter;
//}

//MangaDoublePageMode TNekoDriver::doublePageMode()
//{
//    return fDoublePageMode;
//}

//bool TNekoDriver::mangaOptimizeMode()
//{
//    return fMangaOptimizeMode;
//}

void TNekoDriver::SetPageJpegMode( bool jpegmode )
{
    fJpegMode = jpegmode;
}

void TNekoDriver::SetPageQuality( int quality )
{
    fCodecQuality = quality;
}

void TNekoDriver::SetMangaOptimizeMode( bool gamma, bool sharpen )
{
    fGammaCorrect = gamma;
    fSharpen = sharpen;
}

void TNekoDriver::SetDitherMode( bool dither )
{
    fDitherMode = dither;
}

void TNekoDriver::SetScaleFilter( ScaleFilter filter )
{
    fScaleFilter = filter;
}

void TNekoDriver::SetDoublePageMode( MangaDoublePageMode mode )
{
    fDoublePageMode = mode;
}

void TNekoDriver::SetPageSize( int width, int height )
{
    fStageWidth = width;
    fStageHeight = height;
}

tagPageBundleRec::tagPageBundleRec()
{
    InterlockedExchange((LONG*)&FitImage1, 0);
    InterlockedExchange((LONG*)&FitImage2, 0);
    FitRotated1 = FitRotated2 = FromAnimate = FromGrayscale = Corrupted = DoublePage = Sharpened = Busy = false;
    DoublePageMode = mdpmSingle;
}

void tagPageBundleRec::DropImageStore()
{
    if (FitImage1) {
        delete FitImage1;
        FitImage1 = NULL;
    }
    if (FitImage2) {
        delete FitImage2;
        FitImage2 = NULL;
    }
}

tagBookBundleRec::tagBookBundleRec()
{
    Cover = NULL;
}