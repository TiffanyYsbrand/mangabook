Sub Create2()
'
' Create2 Macro
'
'
    Application.WindowState = wdWindowStateMinimize
    Application.WindowState = wdWindowStateNormal
    Selection.Style = ActiveDocument.Styles("Heading 1")
    Selection.TypeText Text:="Chapte1"
    Selection.MoveLeft Unit:=wdCharacter, Count:=1
    Selection.TypeText Text:="r"
    Selection.EndKey Unit:=wdLine
    Selection.TypeParagraph
    Selection.TypeText Text:="content1"
    Selection.TypeParagraph
    Selection.Style = ActiveDocument.Styles("Heading 1")
    Selection.TypeText Text:="Chapter2"
    Selection.TypeParagraph
    Selection.TypeText Text:="content2"
    ActiveDocument.AttachedTemplate.BuildingBlockEntries("Automatic Table 1"). _
        Insert Where:=Selection.Range, RichText:=True
    ChangeFileOpenDirectory _
        "G:\root\Projects\MyPrj\MangaBook\MangaBook\PpsDES\"
    ActiveDocument.SaveAs FileName:="Chapter1.docx", FileFormat:= _
        wdFormatXMLDocument, LockComments:=False, Password:="", AddToRecentFiles _
        :=True, WritePassword:="", ReadOnlyRecommended:=False, EmbedTrueTypeFonts _
        :=False, SaveNativePictureFormat:=False, SaveFormsData:=False, _
        SaveAsAOCELetter:=False
End Sub
