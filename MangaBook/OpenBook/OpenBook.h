#ifndef _OPEN_BOOK_H
#define _OPEN_BOOK_H

#include <QtCore>
#include <QVector>
#include <QList>
#include <QDataStream>

#pragma pack(push, 1)
typedef struct tagVMBR {
    tagVMBR();
    char Magic[8];  // SNBP 000B
    qint32 Rev80;   // 00 00 80 00
    qint32 RevA3;   // 00 A3 A3 A3
    qint32 RevZ1;   // 00 00 00 00
    qint32 FileCount;       // 00 00 00 0F
    qint32 VFatSize;        // 00 00 01 A1
    qint32 VFatCompressed;  // 00 00 00 AC
    qint32 BinaryStreamSize;        // 00 09 0C D6
    qint32 PlainStreamSize;         // 00 00 05 A8
    qint32 RevZ2;   // 00 00 00 00
} TVMBR;
typedef struct tagVFatRec {
    quint32 Attribute;
    quint32 FilenameOffset;
    quint32 OriginalFileSize;
} TVFatRec;
typedef struct tagVTailRec {
    quint32 BlockIndex;
    quint32 ContentOffset;
} TVTailRec;
typedef struct tagVEOMRec {
    tagVEOMRec();
    qint32 TailSize;
    qint32 TailOffset;
    char Magic[8];  // SNBP 000B
} TVEOMRec;
#pragma pack(pop)

typedef struct tagVFileRec {
    QByteArray VPath;
    bool IsBinary;
    qint64 Offset;
    int Size;
    int UnsortIndex;
} TVFileRec;

typedef struct tagDummyFileRec {
    bool IsBinary;
    QString VPath;
    QByteArray Content;
} TDummyFileRec;

typedef QVector < TVFileRec > TVFiles;
typedef QList <TDummyFileRec> TDummyFiles;

class OpenBookWriter {
private:
    TDummyFiles fUnsortFiles;
    TVFiles fVirtualFiles;
    QByteArray fBinaryCache, fPlainCache;
    int fBinarySize;
private:
    bool FlushUnsortBuffer();
public:
    bool AddBinaryFile(QString filename, const QByteArray& content);
    bool AddPlainFile(QString filename, const QByteArray& content);
    bool OutputSndaBook(QIODevice* file);
};

class OpenBookReader {
private:
    TDummyFiles fUnsortFiles;
    //TVFiles fVirtualFiles;
    QByteArray fBinaryCache, fPlainCache;
private:
    bool FillUnsortBuffer();
public:
    bool DumpUnsortFiles(QString xmlfolder);
    bool LoadSndaBook(QIODevice* file);
};


QDataStream &operator<<(QDataStream &out, const TVMBR &mbr);
QDataStream &operator<<(QDataStream &out, const TVFatRec &fat);
QDataStream &operator<<(QDataStream &out, const TVTailRec &tail);
QDataStream &operator<<(QDataStream &out, const TVEOMRec &eom);

QDataStream &operator>>(QDataStream &in, TVMBR &mbr);
QDataStream &operator>>(QDataStream &in, TVFatRec &fat);
QDataStream &operator>>(QDataStream &in, TVTailRec &tail);
QDataStream &operator>>(QDataStream &in, TVEOMRec &eom);


QByteArray qCompressBz(const uchar* data, int nbytes, int compressionLevel);
QByteArray qUncompressBz(const uchar* data, int nbytes, int outputbytes);


#endif