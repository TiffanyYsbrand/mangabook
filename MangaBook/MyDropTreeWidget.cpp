#include "MyDropTreeWidget.h"
#include <QDragEnterEvent>
#include <QDragLeaveEvent>
#include <QDragMoveEvent>
#include <QUrl>
#include <QHeaderView>


MyDropTreeWidget::MyDropTreeWidget( QWidget* parent ) : QTreeWidget(parent)
{
    setAcceptDrops(true); // override Designer settings?
    this->header()->setClickable(true);
    ContentSortingEnabled = false;
    QObject::connect((QObject*)this->header(), SIGNAL(sectionClicked(int)),
        this, SLOT(headerClicked(int)));
    //QObject::connect((QObject*)this->header(), SIGNAL(sortIndicatorChanged(int, Qt::SortOrder)),
    //    this, SLOT(headerSorted(int, Qt::SortOrder)));
}

void MyDropTreeWidget::dragEnterEvent( QDragEnterEvent *event )
{
    const QMimeData* data = event->mimeData();
    if (data->hasUrls()) {
        if (data->urls().isEmpty() == false) {
            // we only accept files / dir
            event->acceptProposedAction();
            //emit dropChanged(event->mimeData());
        }
    }
}

void MyDropTreeWidget::dragMoveEvent( QDragMoveEvent *event )
{
    event->acceptProposedAction();
}

void MyDropTreeWidget::dragLeaveEvent( QDragLeaveEvent *event )
{
    event->accept();
}

void MyDropTreeWidget::dropEvent( QDropEvent *event )
{
    event->acceptProposedAction();
    emit dropChanged(event->mimeData());
}

void MyDropTreeWidget::clearDrop()
{

}

void MyDropTreeWidget::headerClicked(int index)
{
    if (ContentSortingEnabled == false) {
        ContentSortingEnabled = true;
        this->header()->setSortIndicatorShown(true);
        // wait for sort
        Order = Qt::AscendingOrder;
        this->sortItems(index, Order);
    } else if (Order == Qt::DescendingOrder) {
        ContentSortingEnabled = false;
        this->header()->setSortIndicatorShown(false);
        emit sortDisabled(index);
    } else if (Order == Qt::AscendingOrder) {
        Order = Qt::DescendingOrder;
        this->sortItems(index, Order);
    }
}

void MyDropTreeWidget::keyReleaseEvent( QKeyEvent *event )
{
    if (event->key() == Qt::Key_Delete) {
        if (event->modifiers() == Qt::ShiftModifier) {
            emit deleteRequest(true); // Debug friendly
        } else {
            emit deleteRequest(false);
        }
    }
}