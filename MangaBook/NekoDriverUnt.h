#ifndef _NEKO_DRIVER_UNIT_H
#define _NEKO_DRIVER_UNIT_H

#include "BookDepository.h"
#include "MainUnt.h"
#include <QFile>
#include <QTreeWidget>
#include <QtGui>
#include <QList>
#include <QMap>
#include <QImage>

enum MangaDoublePageMode {
    mdpmSingle,
    mdpmAutoNippon,
    mdpmSmartNippon,
    mdpmAutoNative,
    mdpmSmartNative
};

enum ScaleFilter {
    sfAuto,
    sfQt,
    sfLanczos3,
    sfQuadRatic
};

typedef struct tagExtraInfoRec {
    QString filename;
    int fileindex, allfilecount;
    int loadtime, decodetime, scaletime, drawtime;
    int codecindex;
    int filesize, originalwidth, originalheight;
    bool fitrotated;
} TExtraInfoRec;

typedef struct tagPageBundleRec {
    tagPageBundleRec();
    void DropImageStore();
    QString Fullpath; // C:\d.jpg, a.zip|a/b.jpg
    QString Semipath; // d, b
    QString Folderpath; // C:\, a.zip
    bool FromArchive, FromGrayscale, FromAnimate, FitRotated1, FitRotated2, DoublePage, Sharpened, Corrupted, Busy;
    long long StreamOffset, StreamSize, SourceSize;
    TStreamCodec StreamCodec;
    QImage *FitImage1, *FitImage2; // QAtomicPointer
    QByteArray FitFile1, FitFile2;
    bool FitJPEG1, FitJPEG2;
    TExtraInfoRec ExtraInfo;
    MangaDoublePageMode DoublePageMode;
} TPageBundleRec;

typedef QList < TPageBundleRec > PageBundleList;

typedef struct tagChapterBundleRec {
    QString Chaptername;
    PageBundleList Pages;
    int NewChapterIndex;
    MangaDoublePageMode DoublePageMode;
} TChapterBundleRec;

typedef QList < TChapterBundleRec > ChapterBundleList;

typedef struct tagBookBundleRec {
    tagBookBundleRec();
    QString BookTitle;
    QString Author;
    QString Generator;
    QString Brief;
    QImage *Cover;
} TBookBundleRec;

class TNekoDriver: public QObject {
    Q_OBJECT
public:
    explicit TNekoDriver(TMainFrm *frm);
public:
    typedef QMap < QString, int > QStringIntMap;
protected:
    BookDepository fBookDepository;
private:
    QString fProjectViewNameFilter;
    QStringIntMap fDropedChapterLookupTable;
    QIcon fImageIcon, fDelIcon, fCloakingIcon;
    QIcon fFolderIcon, fFolderIconOpened;
    TBookBundleRec fBookRec;
    ChapterBundleList fChapters;
    bool fGammaCorrect, fSharpen, fDitherMode, fJpegMode;
    MangaDoublePageMode fDoublePageMode;
    ScaleFilter fScaleFilter;
    QMutex fChapterMute;
    int fBusyPageCount, fFinishPageCount;
    int fStageWidth, fStageHeight;
    int fCodecQuality;
private:
    static bool NicetoScaleOnFly( QSize &orgsize, bool ispng, bool isjpeg );
    static bool NiceToRotate( int width, int height, int destwidth, int destheight );
    static bool PrepareScaledQPicture( QImage& qimage, int destwidth, int destheight, ScaleFilter filter, int& scaletime );
    //static void OptimizeMangaImage( QImage& qimage );
    void SaveBundleRec( TPageBundleRec& page );
    bool PreparePageImagesBuffer();
public:
    bool IsProjectEmpty();
    bool IsProjectModified();
    bool BuildProjectFromSnb(QString snbfilename);
    bool LoadProjectFromFile(QString bookfilename);
    bool SaveProjectToFile(QString bookfilename, bool compress);
    bool KeepProjectView(QTreeWidget *treeview);
    bool ApplyProjectViewNameFilter(QString newfilter);
    bool DumpRefBinaray(int index, QIODevice *file, bool onlystock);
    bool DumpProjectContent(QString outputpath, QLabel *label = 0, QProgressBar* bar = 0);
    bool BuildBookFromBuffer(QString snbfilename, QProgressBar* bar = 0); // OpenPacker
    bool PrepareBufferStock(QLabel *label = 0, QProgressBar* bar = 0); // MViewLite Scale
    bool ReplaceBundleRecContent(int index, QByteArray& newcontent);
    bool MarkPageAsDeleted(int index);
    bool ClearPageDeletedMark(int index);
    bool RollbackToStockSlot(int index);
    int GetAutoChapterIndex(QString folder);
    int AddAutoChapter(QString folder, QString name); // return index
    bool CleanAutoChapterLookupTable();
    int AddChapter(QString name);
    bool RenameChapter(int index, QString newname);
    QString GetChapterName(int index);
    bool RemoveChapter(int index);
    bool ClearAllChapters();
    bool SortChapters(PageSortingMethod method);
    bool SortPages(int chapterindex, PageSortingMethod method);
    bool SortAutoChapterPages(PageSortingMethod method);
    bool MoveupChapters(QSet < int > indexes);
    bool MovedownChapters(QSet < int > indexes);
    bool AddPageToChapter(int chapterindex, QString filename); // Failed if image corrupted
    bool AddArchivedPagesToChapter(int chapterindex, QString filename); // PKArchive
    bool RemovePage(int chapterindex, int pageindex);
    void SetBookInfo(const QString& title, const QString& author, const QString& brief);
    const QImage* GetCover();
    void SetCover(const QString& filename);
    void SetCover(const QImage* cover);
    static void ScaleCover(QImage* cover);
    static void BuildSinglePrefetch( TPageBundleRec& page, int destwidth, int destheight, ScaleFilter filter, MangaDoublePageMode doublepagemode, bool crop, bool exposure, bool surface, bool artifacts, bool gamma, bool sharpen );
    //ScaleFilter filter();
    //MangaDoublePageMode doublePageMode();
    //bool mangaOptimizeMode();
    void SetMangaOptimizeMode(bool gamma, bool sharpen);
    void SetDitherMode(bool dither);
    void SetScaleFilter(ScaleFilter filter);
    void SetDoublePageMode(MangaDoublePageMode mode);
    void SetPageJpegMode(bool jpegmode);
    void SetPageQuality(int quality);
    void SetPageSize(int width, int height);
public slots:
    void onPageStarted(int chapterindex, int pageindex);
    void onPageFinished(int chapterindex, int pageindex);
signals:
    void prepareBufferProgressChanged(int percent);
    void prepareBufferStageChanged(QString stage);
    void prepareBufferFinished();
};

class ScalerThread : public QThread
{
    Q_OBJECT
public:
    explicit ScalerThread(ChapterBundleList& chapters, QMutex& mutex, ScaleFilter filter, bool gamma, bool sharpen, bool dither, bool crop, bool exposure, bool surface, bool artifacts, int width, int height);
private:
    ChapterBundleList& fChapters;
    QMutex& fChapterMutex;
    ScaleFilter fScaleFilter;
    bool fGammaCorrect, fSharpen, fDitherMode;
    bool fAutoCrop, fAutoExposure, fSurfaceBlur, fRemoveArtifacts;
    int fStageWidth, fStageHeight;
protected:
    void run();
signals:
    void startScale(int chapterindex, int pageindex);
    void finishScale(int chapterindex, int pageindex);
};

typedef TNekoDriver* PNekoDriver;
extern PNekoDriver theNekoDriver;

#endif