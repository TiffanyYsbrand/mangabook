#ifndef _BOOK_DEPOSITORY_H
#define _BOOK_DEPOSITORY_H

#include <QtCore>
#include <QtCore\QVector>
#include <QtCore\QMap>
#include "Common.h"

typedef struct tagPageRecItem {
    QString ImageVPath;
    QByteArray FileContent;
} TPageRecItem, *PPageRecItem;

typedef struct tagChapterRecItem {
    QString ChapterTitle;
    bool HideTitle;
    QVector < TPageRecItem > Pages;
    QByteArray XmlContent;
} TChapterRecItem, *PCharpterRecItem;

typedef struct tagTopicRecItem {
    QVector < TChapterRecItem > Chapters;
    QByteArray XmlContent;
} TTopicRecItem, *PTopicRecItem;

typedef struct tagBookRecItem {
    QString BookTitle;
    QString Author;
    QString Generator;
    QString Brief;
    TPageRecItem Cover;
    QByteArray XmlContent;
} TBookRecItem, *PBookRecItem;

class BookDepository : public QObject {
    Q_OBJECT
public:
    BookDepository();
private:
    TBookRecItem fBookRec; // for xml
    TTopicRecItem fTopicRec; // for xml
protected:
    bool fProjectModified;
private:
    int __cdecl logprintf(const char * _Format, ...);
    void AddLog( QString content, TLogType logtype );
public:
    int AddChapter(const QString& Title);
    bool AddPageIntoChapter(int index, const QByteArray& content, bool jpeg);
    void SetBookInfo(const QString& Title, const QString& Author, const QString& Brief);
    void SetCover(QImage* image);
    bool CleanupAllBuffers();
    bool GenerateBookFromBuffer(const QString& snbfilename);
signals:
    void logStored(QString content, TLogType logtype);
    void snbProgressChanged(int percent);
};

bool RemoveXmlDocumentMark(QByteArray& xmlcontent);

#endif
