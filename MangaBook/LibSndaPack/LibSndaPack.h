


////////////////////////////////////////////////////////////////////////////////////////////////////
// file: LibSndaPack.h
// date: September 23 2010
// font: consolas,9pt
// 
// Interface defination for Pack Dll
////////////////////////////////////////////////////////////////////////////////////////////////////


#ifndef _LIB_SNDAPACK
#define _LIB_SNDAPACK


////////////////////////////////////////////////////////////////////////////////////////////////////
#include <Windows.h>




namespace PackBinaryInterface {
extern "C" bool __cdecl PackInitialize();
extern "C" void __cdecl PackTerminate();
extern "C" int __cdecl PackPlainFromDir(char* book, char* folder);
extern "C" int __cdecl UnpackPlain(char *book, char *file, char *dest);
}


#endif // _LIB_SNDAPACK