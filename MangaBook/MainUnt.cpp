#include "MainUnt.h"
#include "ui_MainFrm.h"
#include <QFile>
#include "DbCentre.h"
#include <QDebug>
#include <QBuffer>
#include <Windows.h>
#include "NekoDriverUnt.h"
#include "AddonFuncUnt.h"
#include <QRegExp>
#include <QInputDialog>
#include "NewBookUnt.h"
#include "SettingsUnt.h"
#include <QtGUI/QDesktopServices>
#include <QSettings>
#include <QDomDocument>


TMainFrm::TMainFrm(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::TMainFrm)
    , fDiscardRenameCheck(false)
    , fCloudAppID("7a6229515e65f03adf5f65705b367a99")
{
    ui->setupUi(this);
    LoadAppSettings();

    // Additionally Initialize
    if (StateSetting.WindowMaxium) {
        setWindowState(Qt::WindowMaximized);
    }
    if (StateSetting.MainFrmState.isEmpty() == false) {
        restoreState(StateSetting.MainFrmState);
    }
    if (StateSetting.ProjectLayoutState.isEmpty() == false) {
        ui->splitterforproject->restoreState(StateSetting.ProjectLayoutState);
    }
    if (StateSetting.MessageLayoutState.isEmpty() == false) {
        ui->splitterforoutput->restoreState(StateSetting.MessageLayoutState);
    }
    ui->logView->horizontalHeader()->resizeSection(0, 60);
    ui->logView->horizontalHeader()->resizeSection(1, 80);

    // chibimaho do koneko
    // TODO: move initialize to main
    theNekoDriver = new TNekoDriver(this);
    theNekoDriver->SetDoublePageMode(MangaDoublePageMode(GlobalSetting.SplitPage));
    fBookInfo.EnhanceLevel = GlobalSetting.EnhanceLevel;
    fBookInfo.InternalCodec = GlobalSetting.OutputFormat;
    fBookInfo.PageSize = GlobalSetting.OutputResolution;

    // QtDesigner const bug
    //QObject::connect(ui.projectView, SIGNAL(dropChanged(const QMimeData*)),
    //    this, SLOT(onProjectDropfiles(const QMimeData*)));
    QApplication::setApplicationName(tr("MangaBook"));

#if defined(Q_OS_WIN32) || defined(Q_OS_WIN64)
    QSettings settings("HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Uninstall\\Bambook", QSettings::NativeFormat);
    QString cloundlibrarypath = settings.value("AutoRunExec").toString();
    if (cloundlibrarypath.isEmpty()) {
        return;
    }
    QString settingfile = QFileInfo(cloundlibrarypath).path() + "/bbapps.xml";

    QDomDocument xmldoc;
    QFile file(settingfile);
    QString apppath = QApplication::applicationFilePath().replace('/', '\\'); // TODO: toNativePath
    bool hited = false, touched = false;
    if (file.exists() && file.open(QIODevice::ReadOnly)) {
        if (xmldoc.setContent(&file)) {
            QDomElement	apps = xmldoc.documentElement(); // root is bbapps;
            QDomNode app = apps.firstChild();
            while (app.isNull() == false)
            {
                if (app.firstChildElement("id").firstChild().nodeValue().compare(fCloudAppID, Qt::CaseInsensitive) == 0) {
                    hited = true;
                    if (app.firstChildElement("exec").firstChild().nodeValue() != apppath) {
                        app.firstChildElement("exec").firstChild().setNodeValue(apppath);
                        touched = true;
                    }
                }
                app = app.nextSibling();
            }
        }
        file.close();
    } else {
        QDomElement	apps = xmldoc.createElement("bbapps");
        xmldoc.appendChild(apps);
    }
    if (hited == false) {
        QDomElement	apps = xmldoc.documentElement();
        if (apps.tagName().compare("bbapps", Qt::CaseInsensitive) != 0) {
            apps.setTagName("bbapps"); // maybe never occurred
        }
        QDomElement app = xmldoc.createElement("app");
        QDomElement id = xmldoc.createElement("id");
        id.appendChild(xmldoc.createTextNode(fCloudAppID));
        QDomElement exec = xmldoc.createElement("exec");
        exec.appendChild(xmldoc.createTextNode(apppath));
        app.appendChild(id);
        app.appendChild(exec);
        apps.appendChild(app);
        touched = true;
    }
    if (touched && file.open(QIODevice::WriteOnly)) {
        QTextStream stream(&file);
        stream << xmldoc.toString(0); // sanda style
        file.close();
    }
#endif
#ifndef _DEBUG
    // Nuke debug functions
    ui->actionBookToFolder->setVisible(false);
    ui->actionFolderToBook->setVisible(false);
#endif
}

TMainFrm::~TMainFrm()
{
    StateSetting.WindowMaxium = windowState() == Qt::WindowMaximized;
    StateSetting.MainFrmState = saveState();
    StateSetting.ProjectLayoutState = ui->splitterforproject->saveState();
    StateSetting.MessageLayoutState = ui->splitterforoutput->saveState();

    SaveAppSettings();
    delete theNekoDriver;
    delete ui;
}

void TMainFrm::changeEvent(QEvent *e)
{
    QMainWindow::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}

void TMainFrm::StoreTranslator( QTranslator* translator )
{
    fTranslator = translator;
}

void TMainFrm::onFileExitClicked()
{
    this->close();
}

void TMainFrm::onFileOpenClicked()
{
    QString mbkfilename = OpenSaveMpkDialog.getOpenFileName(this, tr("Open MangaBook Project File"), PathSetting.LastProjectFolder, "MangaBook Projects (*.mbk);;All Files (*.*)", 0, 0);
    if (mbkfilename.isEmpty()) {
        return;
    }
    if (theNekoDriver->LoadProjectFromFile(mbkfilename) == false) {
        return;
    }
    fLastSavedProjectFilename = mbkfilename;
    // Project Build Success
    theNekoDriver->KeepProjectView(ui->chaptersView);
    PathSetting.LastProjectFolder = QFileInfo(mbkfilename).path();
    //setWindowFilePath(QFileInfo(mbkfilename).fileName());
    setWindowTitle(QApplication::applicationName() + " - " + QFileInfo(mbkfilename).fileName() + "[*]");
    setWindowModified(false);
}

void TMainFrm::onFileCloseClicked()
{
    if (theNekoDriver->ClearAllChapters()) {
        theNekoDriver->KeepProjectView(ui->chaptersView);
    }
    fLastSavedProjectFilename.clear();
    setWindowTitle(QApplication::applicationName());
    setWindowModified(false);
}

void TMainFrm::onFileSaveClicked()
{
    if (fLastSavedProjectFilename.isEmpty() == false) {
        theNekoDriver->SaveProjectToFile(fLastSavedProjectFilename, false);
        setWindowModified(false);
    } else {
        // ask one
        onFileSaveAsClicked();
    }
}

void TMainFrm::onFileSaveAsClicked()
{
    QString mbkfilename = OpenSaveMpkDialog.getSaveFileName(this, tr("Save MangaBook Project File as"), PathSetting.LastProjectFolder, "MangaBook Projects (*.mbk);;All Files (*.*)", 0, 0);
    if (mbkfilename.isEmpty()) {
        return;
    }
    // TODO: Compress / uncompress project
    theNekoDriver->SaveProjectToFile(mbkfilename, false);
    setWindowModified(false);
    fLastSavedProjectFilename = mbkfilename;
    PathSetting.LastProjectFolder = QFileInfo(mbkfilename).path();
    //setWindowFilePath(QFileInfo(mbkfilename).fileName());
    setWindowTitle(QApplication::applicationName() + " - " + QFileInfo(mbkfilename).fileName() + "[*]");
}

void TMainFrm::onFileNewClicked()
{
    // Reset project?
    if (theNekoDriver->ClearAllChapters()) {
        theNekoDriver->KeepProjectView(ui->chaptersView);
    }
    fLastSavedProjectFilename.clear();
    setWindowTitle(QApplication::applicationName() + " - Untitled Book.mbk[*]");
    setWindowModified(true);
}

void TMainFrm::onFolderToBookClicked()
{
    QString xmlfolder = OpenSaveFileDialog.getExistingDirectory(this, tr("Browse XML Folder"), PathSetting.LastImportFolder);
    if (xmlfolder.isEmpty()) {
        return;
    }
    PathSetting.LastImportFolder = xmlfolder;

    if (QFile::exists(xmlfolder + "/snbf/book.snbf") == false || QFile::exists(xmlfolder + "/snbf/toc.snbf") == false) {
        writeLog(QString(tr("\"%1\" is not a valid snbf folder.")).arg(xmlfolder));
    }

    QString bookfilename = OpenSaveFileDialog.getSaveFileName(this, tr("Save Manga As"), PathSetting.LastCustomBookFolder + "/" + QDir(xmlfolder).dirName() + ".snb", "snb Files (*.snb);;All Files (*.*)", 0, 0);
    if (bookfilename.isEmpty()) {
        return;
    }
    PathSetting.LastCustomBookFolder = QFileInfo(bookfilename).path();

    //if (PackInitialize()) {
    //    int result = PackPlainFromDir(bookfilename.toAscii().data(), xmlfolder.toAscii().data());
    //    PackTerminate();
    //}
    ui->actionFolderToBook->setVisible(false);
    QFileInfo info(xmlfolder);
    OpenBookWriter writer;
    int baselen = xmlfolder.length();
    if (xmlfolder.right(1) != "/") {
        baselen++;
    }
    int acceptcount = 0, bypassoucnt = 0;

    TryAcceptXmlFolder(info, &writer, baselen, 8, bypassoucnt, acceptcount);

    QFile file(bookfilename);
    file.open(QIODevice::WriteOnly);
    writer.OutputSndaBook(&file);
    file.close();
    writeLog(QString(tr("%1 files packaged into %2. %3 rejected.")).arg(acceptcount).arg(QFileInfo(bookfilename).fileName()).arg(bypassoucnt));
    ui->actionFolderToBook->setVisible(true);
}

void TMainFrm::onBookToFolderClicked()
{
    QString bookfilename = OpenSaveFileDialog.getOpenFileName(this, tr("Open Manga File"), PathSetting.LastStockBookFolder, "snb Files (*.snb);;All Files (*.*)", 0, 0);
    if (bookfilename.isEmpty()) {
        return;
    }
    PathSetting.LastStockBookFolder = QFileInfo(bookfilename).path();

    QString xmlfolder = OpenSaveFileDialog.getExistingDirectory(this, tr("Save To XML Folder"), PathSetting.LastExportFolder);
    if (xmlfolder.isEmpty()) {
        return;
    }
    PathSetting.LastExportFolder = xmlfolder;

    //if (PackInitialize()) {
    //    int result = UnpackPlain(bookfilename.toAscii().data(), "snbc/images/0.jpg", jpgfilename.toAscii().data());
    //    PackTerminate();
    //}
    ui->actionBookToFolder->setVisible(false);
    OpenBookReader reader;
    QFile file(bookfilename);
    file.open(QIODevice::ReadOnly);
    if (reader.LoadSndaBook(&file)) {
        reader.DumpUnsortFiles(xmlfolder);
    }
    file.close();
    ui->actionBookToFolder->setVisible(true);
}

void TMainFrm::TryAcceptDrop( QFileInfo &info, int maxdepth, int &bypasscount, int &acceptcount )
{
    maxdepth--;
    if ((info.exists() == false)) {
        return;
    }
    if (info.isDir()) {
        // TODO: read files from it
        QDir dir(info.filePath());
        dir.setFilter(QDir::Files | QDir::Dirs | QDir::NoSymLinks | QDir::NoDotAndDotDot);
        dir.setSorting(QDir::DirsLast);

        QFileInfoList list = dir.entryInfoList();
        foreach(QFileInfo subinfo, list) {
            if (maxdepth > 0) {
                TryAcceptDrop(subinfo, maxdepth, bypasscount, acceptcount);
            }
        }
    }
    if (info.isFile()) {
        TImageType mimetype = GuessMIMEType(info.filePath());
        if (mimetype == itUnknown || mimetype == itRarArchive) {
            bypasscount++;
            return;
        }
        if (mimetype == itPKArchive) {
            int chapterindex = theNekoDriver->AddAutoChapter(info.filePath(), CleanupTags(info.completeBaseName()));
            if (chapterindex == -1) {
                bypasscount++;
                return;
            }
            if (theNekoDriver->AddArchivedPagesToChapter(chapterindex, info.filePath())) {
                acceptcount++;
            } else {
                bypasscount++;
            }
            return;
        }
        // auto chapter may exists
        int chapterindex = theNekoDriver->GetAutoChapterIndex(info.path());
        if (chapterindex == -1) {
            chapterindex = theNekoDriver->AddAutoChapter(info.path(), CleanupTags(QFileInfo(info.path()).completeBaseName()));
        }
        if (chapterindex == -1) {
            bypasscount++;
            return;
        }
        if (theNekoDriver->AddPageToChapter(chapterindex, info.filePath())) {
            acceptcount++;
        } else {
            bypasscount++;
        }
    }
}

void TMainFrm::TryAcceptXmlFolder( QFileInfo &info, OpenBookWriter* writer, int baselen, int maxdepth, int &bypasscount, int &acceptcount )
{
    maxdepth--;
    if ((info.exists() == false)) {
        return;
    }
    if (info.isDir()) {
        // TODO: read files from it
        QDir dir(info.filePath());
        dir.setFilter(QDir::Files | QDir::Dirs | QDir::NoSymLinks | QDir::NoDotAndDotDot);
        dir.setSorting(QDir::DirsLast);

        QFileInfoList list = dir.entryInfoList();
        foreach(QFileInfo subinfo, list) {
            if (maxdepth > 0) {
                TryAcceptXmlFolder(subinfo, writer, baselen, maxdepth, bypasscount, acceptcount);
            }
        }
    }
    if (info.isFile()) {
        TImageType mimetype = GuessMIMEType(info.filePath());
        QString vpath = info.filePath().right(info.filePath().size() - baselen);
        QFile file(info.filePath());
        file.open(QIODevice::ReadOnly);
        if (mimetype == itUnknown) {
            // plain
            writer->AddPlainFile(vpath, file.readAll());
        } else {
            writer->AddBinaryFile(vpath, file.readAll());
        }
        acceptcount++;
    }
}

void TMainFrm::TryAcceptPages( QFileInfo &info, int maxdepth, int chapterindex, int &bypasscount, int &acceptcount )
{
    maxdepth--;
    if ((info.exists() == false)) {
        return;
    }
    if (info.isDir()) {
        // we'd never go here
        // TODO: read files from it
        QDir dir(info.filePath());
        dir.setFilter(QDir::Files | QDir::Dirs | QDir::NoSymLinks | QDir::NoDotAndDotDot);
        dir.setSorting(QDir::DirsLast);

        QFileInfoList list = dir.entryInfoList();
        foreach(QFileInfo subinfo, list) {
            if (maxdepth > 0) {
                TryAcceptPages(subinfo, maxdepth, chapterindex, bypasscount, acceptcount);
            }
        }
    }
    if (info.isFile()) {
        TImageType mimetype = GuessMIMEType(info.filePath());
        if (mimetype == itUnknown || mimetype == itRarArchive) {
            bypasscount++;
            return;
        }
        if (mimetype == itPKArchive) {
            if (theNekoDriver->AddArchivedPagesToChapter(chapterindex, info.filePath())) {
                acceptcount++;
            } else {
                bypasscount++;
            }
            return;
        }
        if (theNekoDriver->AddPageToChapter(chapterindex, info.filePath())) {
            acceptcount++;
        } else {
            bypasscount++;
        }
    }
}

void TMainFrm::onChapterDropfiles( const QMimeData* data /*= 0*/ )
{
    if (data == NULL) {
        return;
    }
    int acceptcount = 0, bypassoucnt = 0;
    int costtime = -1;
    // try replace directory / files
    if (data->hasUrls()) {
        if (data->urls().isEmpty() == false) {
            // find match
            QTime starttime = QTime::currentTime();
            foreach (QUrl url, data->urls()) {
                QString path = url.toLocalFile();
                QFileInfo info(path);
                TryAcceptDrop(info, 8, bypassoucnt, acceptcount); // Top call for TryAcceptDrop
            }
            costtime = starttime.msecsTo(QTime::currentTime());
        }
    }
    writeLog(QString(tr("%1 files added into book. %2 rejected.")).arg(acceptcount).arg(bypassoucnt));
    if (costtime != -1) {
        writeLog(QString(tr("we take %1 ms for dropped files.")).arg(costtime), ltHint);
    }
    // TODO: short auto chapters
    if (GlobalSetting.PageSorting != psmNone) {
        theNekoDriver->SortAutoChapterPages(GlobalSetting.PageSorting);
    }
    theNekoDriver->CleanAutoChapterLookupTable();
    if (acceptcount > 0) {
        fDiscardRenameCheck = true;
        theNekoDriver->KeepProjectView(ui->chaptersView);
        fDiscardRenameCheck = false;
        setWindowModified(true);
    }
}


BookInfo::BookInfo()
{
    Title = QObject::tr("Manga");
    Author = QObject::tr("Sample Author");
    Brief = QObject::tr("Sample Brief");
    CoverPath.clear();
    PageSize = rmFullScreen;
    InternalCodec = cmJPEGVeryHigh;
    EnhanceLevel = fmHigh;
}

void TMainFrm::onBuildBookClicked()
{
    if (theNekoDriver->IsProjectEmpty()) {
        return;
    }

    BookInfo newbookinfo = fBookInfo;
    newbookinfo.Title = theNekoDriver->GetChapterName(0);
    if (newbookinfo.Title.isEmpty()) {
        newbookinfo.Title = tr("Manga");
    }
    if (newbookinfo.Author.isEmpty()) {
        newbookinfo.Author = tr("Sample Author");
    }
    if (newbookinfo.Brief.isEmpty()) {
        newbookinfo.Brief = tr("Sample Brief");
    }
    if (newbookinfo.Cover.isNull() && theNekoDriver->GetCover() != NULL) {
        newbookinfo.Cover = *theNekoDriver->GetCover();
    }

    TNewBookFrm *managerfrm = new TNewBookFrm();
    managerfrm->StoreBookInfo(newbookinfo);
    int ret = managerfrm->exec();
    if (ret == QDialog::Accepted) {
        fBookInfo = managerfrm->GetBookInfo();
    }
    delete managerfrm;
    if (ret != QDialog::Accepted) {
        return; // Keep fBookInfo untouched
    }

    theNekoDriver->SetBookInfo(fBookInfo.Title, fBookInfo.Author, fBookInfo.Brief);
    theNekoDriver->SetCover(&fBookInfo.Cover);
    if (fBookInfo.InternalCodec == cmPNG) {
        theNekoDriver->SetPageJpegMode(false);
        theNekoDriver->SetPageQuality(80);
    } else {
        theNekoDriver->SetPageJpegMode(true);
        if (fBookInfo.InternalCodec == cmJPEGVeryHigh) {
            theNekoDriver->SetPageQuality(85);
        } else {
            theNekoDriver->SetPageQuality(70);
        }
    }
    if (fBookInfo.EnhanceLevel == fmNone) {
        theNekoDriver->SetMangaOptimizeMode(false, false);
    } else if (fBookInfo.EnhanceLevel == fmSlight) {
        theNekoDriver->SetMangaOptimizeMode(true, false);
    } else {
        // Hight n maximum
        theNekoDriver->SetMangaOptimizeMode(true, true);
    }
    theNekoDriver->SetScaleFilter(fBookInfo.EnhanceLevel == fmNone?sfAuto:sfLanczos3);
    theNekoDriver->SetDitherMode(fBookInfo.EnhanceLevel == fmMaximum);
    if (fBookInfo.PageSize == rmSafe) {
        theNekoDriver->SetPageSize(540, 690);
    } else if (fBookInfo.PageSize == rmPixelToPixel) {
        theNekoDriver->SetPageSize(540, 700);
    } else if (fBookInfo.PageSize == rmFullScreen) {
        theNekoDriver->SetPageSize(580, 780);
    } else if (fBookInfo.PageSize == rmIPad) {
        theNekoDriver->SetPageSize(768, 1024);
    }

    // Build Project From Stock Snb
    QString bookfilename = OpenSaveFileDialog.getSaveFileName(this, tr("Save Manga As"), QString("%1/%2.snb").arg(PathSetting.LastCustomBookFolder).arg(fBookInfo.Title), "snb Files (*.snb);;All Files (*.*)", 0, 0);
    if (bookfilename.isEmpty()) {
        return;
    }
    PathSetting.LastCustomBookFolder = QFileInfo(bookfilename).path();


    ui->actionBuildBook->setVisible(false);
    action = new QLabel(tr("Prepare Buffer..."));
    action->setMinimumSize(120, 14);
    action->setMaximumSize(200, 18);
    action->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Minimum);
    progress = new QProgressBar();
    progress->setMinimumSize(250, 14);
    progress->setMaximumSize(400, 18);
    progress->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::Minimum);
    progress->setStyle(new QCleanlooksStyle());
    ui->statusBar->addWidget(action);
    ui->statusBar->addWidget(progress);
    fBookfilename = bookfilename;
    QObject::connect(theNekoDriver, SIGNAL(prepareBufferFinished()),
        this, SLOT(onReadyBuildBook()), Qt::QueuedConnection);
    fPrepareBufferStartTime = QTime::currentTime();
    if (theNekoDriver->PrepareBufferStock(action, progress) == false) {
        ui->statusBar->removeWidget(action);
        ui->statusBar->removeWidget(progress);
        ui->actionBuildBook->setVisible(true);
        delete action;
        delete progress;
        return;
    }
    //ui->statusBar->removeWidget(action);
    //ui->statusBar->removeWidget(progress);
}

void TMainFrm::onReadyBuildBook()
{
    QObject::disconnect(theNekoDriver, SIGNAL(prepareBufferFinished()),
        this, SLOT(onReadyBuildBook()));
    action->setText(tr("Compressing..."));

    bool bookbuilded = theNekoDriver->BuildBookFromBuffer(fBookfilename, progress);

    int costtime = fPrepareBufferStartTime.msecsTo(QTime::currentTime());

    writeLog(QString(tr("Build Book %1!")).arg(bookbuilded?QString(tr("Successed in %1 ms").arg(costtime)):tr("Failed")), ltMessage);

    ui->statusBar->removeWidget(action);
    ui->statusBar->removeWidget(progress);

    ui->actionBuildBook->setVisible(true);

    delete action;
    delete progress;
}

void TMainFrm::onChapterWannaRenameValue( QTreeWidgetItem* item, int column )
{
    if (fDiscardRenameCheck || item == NULL || column != 0) {
        return;
    }
    if (item->data(0, Qt::UserRole).toInt() != 0) {
        return;
    }
    int chapterindex = item->data(0, Qt::UserRole + 1).toInt();
    QString newtitle = item->text(0);
    if (theNekoDriver->RenameChapter(chapterindex, newtitle)) {
        ui->chaptersView->resizeColumnToContents(0);
        ui->chaptersView->resizeColumnToContents(1);
        setWindowModified(true);
    }
}


void TMainFrm::onSortChapterClicked()
{
    if (theNekoDriver->SortChapters(GlobalSetting.PageSorting)) {
        theNekoDriver->KeepProjectView(ui->chaptersView);
        setWindowModified(true);
    }
}

void TMainFrm::onMoveupChapterClicked()
{
    // retrieve selected files in projectView
    QList < QTreeWidgetItem* > selecteditems = ui->chaptersView->selectedItems();
    if (selecteditems.isEmpty()) {
        return;
    }
    QSet < int > selectedchapters;
    foreach(QTreeWidgetItem* currentitem, selecteditems) {
        int selectnodetype = currentitem->data(0, Qt::UserRole).toInt();
        if (selectnodetype != 0 && selectnodetype != 1) {
            continue; // never reached
        }
        int chapterindex = currentitem->data(0, Qt::UserRole + 1).toInt();
        if (selectnodetype == 0) {
            // Chapter
            selectedchapters.insert(chapterindex);
        }
    }
    if (theNekoDriver->MoveupChapters(selectedchapters)) {
        theNekoDriver->KeepProjectView(ui->chaptersView);
        setWindowModified(true);
    }

}

void TMainFrm::onMovedownChapterClicked()
{
    // retrieve selected files in projectView
    QList < QTreeWidgetItem* > selecteditems = ui->chaptersView->selectedItems();
    if (selecteditems.isEmpty()) {
        return;
    }
    QSet < int > selectedchapters;
    foreach(QTreeWidgetItem* currentitem, selecteditems) {
        int selectnodetype = currentitem->data(0, Qt::UserRole).toInt();
        if (selectnodetype != 0 && selectnodetype != 1) {
            continue; // never reached
        }
        int chapterindex = currentitem->data(0, Qt::UserRole + 1).toInt();
        if (selectnodetype == 0) {
            // Chapter
            selectedchapters.insert(chapterindex);
        }
    }

    if (theNekoDriver->MovedownChapters(selectedchapters)) {
        theNekoDriver->KeepProjectView(ui->chaptersView);
        setWindowModified(true);
    }
}

void TMainFrm::onAddChapterClicked()
{
    // Autoname?
    bool ok;
    QString text = QInputDialog::getText(this, tr("Input chapter name"),
        tr("Chapter name:"), QLineEdit::Normal,
        tr("New chapter"), &ok);
    if (ok && !text.isEmpty()) {
        if (theNekoDriver->AddChapter(text)) {
            theNekoDriver->KeepProjectView(ui->chaptersView);
            setWindowModified(true);
        }
    }
}

void TMainFrm::onAddPageClicked()
{
    QTreeWidgetItem* currentitem = ui->chaptersView->currentItem();
    if (currentitem == NULL) {
        return;
    }
    int selectnodetype = currentitem->data(0, Qt::UserRole).toInt();
    if (selectnodetype != 0) {
        return; // only continue when select chapter
    }
    int chapterindex = currentitem->data(0, Qt::UserRole + 1).toInt();

    QStringList pagenames = OpenSaveFileDialog.getOpenFileNames(this, tr("Add Pages"), PathSetting.LastSourceFolder, "All Support Files (*.png;*.jpg;*.gif;*.bmp;*.zip);;All Files (*.*)", 0, 0);
    if (pagenames.isEmpty()) {
        return;
    }

    int acceptcount = 0, bypassoucnt = 0;
    int costtime = -1;

    QTime starttime = QTime::currentTime();
    foreach (QString path, pagenames) {
        QFileInfo info(path);
        TryAcceptPages(info, 8, chapterindex, bypassoucnt, acceptcount);
    }
    costtime = starttime.msecsTo(QTime::currentTime());

    writeLog(QString(tr("%1 pages added into book. %2 rejected.")).arg(acceptcount).arg(bypassoucnt));
    if (costtime != -1) {
        writeLog(QString(tr("we take %1 ms for incoming pages.")).arg(costtime), ltHint);
    }
    theNekoDriver->CleanAutoChapterLookupTable();
    if (acceptcount > 0) {
        fDiscardRenameCheck = true;
        theNekoDriver->KeepProjectView(ui->chaptersView);
        fDiscardRenameCheck = false;
        setWindowModified(true);
    }

}

void TMainFrm::onRemoveChapterClicked()
{
    // retrieve selected files in projectView
    QList < QTreeWidgetItem* > selecteditems = ui->chaptersView->selectedItems();
    if (selecteditems.isEmpty()) {
        return;
    }
    bool changed = false;
    QSet < int > nukedchapters;
    QHash < int, int >  nukedpages;
    foreach(QTreeWidgetItem* currentitem, selecteditems) {
        int selectnodetype = currentitem->data(0, Qt::UserRole).toInt();
        if (selectnodetype != 0 && selectnodetype != 1) {
            continue; // never reached
        }
        int chapterindex = currentitem->data(0, Qt::UserRole + 1).toInt();
        if (selectnodetype == 0) {
            // Chapter
            nukedchapters.insert(chapterindex);
        }
        if (selectnodetype == 1) {
            // Page
            int pageindex = currentitem->data(0, Qt::UserRole + 2).toInt();
            nukedpages.insertMulti(chapterindex, pageindex);
        }
    }
    // remove page first
    foreach(int key, nukedpages.uniqueKeys()) {
        if (nukedchapters.contains(key)) {
            continue;
        }
        foreach(int page, nukedpages.values(key)) {
            if (theNekoDriver->RemovePage(key, page)) {
                changed = true;
            }
        }
    }
    // then remove chapter
    foreach(int key, nukedchapters) {
        if (theNekoDriver->RemoveChapter(key)) {
            changed = true;
        }
    }

    if (changed) {
        theNekoDriver->KeepProjectView(ui->chaptersView);
        setWindowModified(true);
    }
}

void TMainFrm::onClearChapterClicked()
{
    if (theNekoDriver->ClearAllChapters()) {
        theNekoDriver->KeepProjectView(ui->chaptersView);
        setWindowModified(true);
    }
}

void TMainFrm::onLanguageEnglishClicked()
{
    QApplication::removeTranslator(fTranslator);
    QFont f = QApplication::font();
    if (f.pointSize() == 9) {
        f.setPointSize(8);
        QApplication::setFont(f);
    }
    ui->retranslateUi(this);
    fLastLangCode = "enu";
}

void TMainFrm::onLanguageChineseClicked()
{
    QApplication::removeTranslator(fTranslator);
    fTranslator->load(QString("MangaBook_chs"), QApplication::applicationDirPath() + "/Language");
    QApplication::installTranslator(fTranslator);
    QFont f = QApplication::font();
    if (f.pointSize() == 8) {
        f.setPointSize(9); // TODO: Simsun 9 in Tahoma 8 fix
        QApplication::setFont(f);
    }
    ui->retranslateUi(this);
    fLastLangCode = "chs";
}

void TMainFrm::onHelpContentsClicked()
{
    QString smartchmname;
    if (fLastLangCode.isEmpty()) {
        smartchmname = QApplication::applicationDirPath() + "/MangaBookManual-" + localLanguage().toUpper() + ".chm";
    } else {
        smartchmname = QApplication::applicationDirPath() + "/MangaBookManual-" + fLastLangCode.toUpper() + ".chm";
    }
    if (QFileInfo(smartchmname).exists() == false) {
        smartchmname = QApplication::applicationDirPath() + "/MangaBookManual-ENU.chm";
    }
    QDesktopServices::openUrl(smartchmname);
}

void TMainFrm::onToolsOptionsClicked()
{
    TSettingsFrm *settingsfrm = new TSettingsFrm();
    settingsfrm->StoreGlobalInfo(GlobalSetting);
    int ret = settingsfrm->exec();
    if (ret == QDialog::Accepted) {
        GlobalSetting = settingsfrm->GetGlobalInfo();

        theNekoDriver->SetDoublePageMode(GlobalSetting.SplitPage);
        fBookInfo.EnhanceLevel = GlobalSetting.EnhanceLevel;
        fBookInfo.InternalCodec = GlobalSetting.OutputFormat;
        fBookInfo.PageSize = GlobalSetting.OutputResolution;
    }
    delete settingsfrm;
}

void TMainFrm::writeLog( QString content, TLogType logtype /*= ltMessage*/ )
{
    if (content.isEmpty()) {
        content = "NULL";
    }
    QStringList list;
    list = content.split("\n");
    for (QStringList::iterator it = list.begin(); it != list.end(); it++) {
        if ((*it).isEmpty() && it + 1 == list.end()) {
            break;
        }
        int prevrowcount = ui->logView->rowCount();
        ui->logView->setRowCount(prevrowcount + 1);
        QTableWidgetItem *item;
        item = new QTableWidgetItem(QDateTime::currentDateTime().toString("hh:mm:ss"));
        ui->logView->setItem(prevrowcount, 0, item);
        item = new QTableWidgetItem(LogTypeToString(logtype));
        ui->logView->setItem(prevrowcount, 1, item);
        item = new QTableWidgetItem(*it);
        ui->logView->setItem(prevrowcount, 2, item);

        ui->logView->scrollToItem(item);
    }
    if (ui->logView->rowCount() < 80) {
        //ui->logView->resizeRowsToContents();
        //ui->logView->resizeColumnsToContents();
    }
}


QString CleanupTags( const QString& source )
{
    // aka GuessChapter
    QString result = source;
    QRegExp exp("\\[.*\\]");
    exp.setMinimal(true);
    result.replace(exp, "");
    if (result.isEmpty()) {
        result = source;
    }
    exp.setPattern("\\(.*\\)");
    result.replace(exp, "");
    if (result.isEmpty()) {
        result = source;
    }
    if (result.trimmed().isEmpty() == false) {
        result = result.trimmed();
    }
    return result;
}

QString LogTypeToString( TLogType logtype )
{
    switch (logtype)
    {
    case ltHint:
        return "Hint";
        break;
    case ltDebug:
        return "Debug";
        break;
    case ltMessage:
        return "Message";
        break;
    case ltError:
        return "Error";
        break;
    }
    return "Unkown";
}

QString localLanguage() {
    QLocale locale = QLocale::system();

    // This switch will translate to the correct locale
    switch (locale.language()) {
        case QLocale::German:
            return "deu";
        case QLocale::French:
            return "fra";
        case QLocale::Chinese:
            return "chs";
        case QLocale::HongKong:
        case QLocale::Macau:
        case QLocale::Taiwan:
            return "cht";
        case QLocale::Spanish:
            return "spa";
        case QLocale::Japanese:
            return "ja";
        case QLocale::Korean:
            return "kor";
        case QLocale::Belgium:
        case QLocale::Netherlands:
        case QLocale::NetherlandsAntilles:
            return "dut";
        default:
            return "enu";
    }
}
