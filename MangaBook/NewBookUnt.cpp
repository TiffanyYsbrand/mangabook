#include "NewBookUnt.h"
#include "ui_NewBookFrm.h"
#include "MainUnt.h"
#include <QFileDialog>
#include "DbCentre.h"
#include "AddonFuncUnt.h"
#include "Filter/QResample.h"



TNewBookFrm::TNewBookFrm(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::TNewBookFrm)
{
    ui->setupUi(this);
}

TNewBookFrm::~TNewBookFrm()
{
    delete ui;
}

void TNewBookFrm::StoreBookInfo( const BookInfo& info )
{
    fBookInfo = info;
    // Load info to control
    ui->titleEdt->setText(fBookInfo.Title);
    ui->authorEdt->setText(fBookInfo.Author);
    ui->briefEdt->setPlainText(fBookInfo.Brief);
    ui->coverEdt->setText(fBookInfo.CoverPath);
    ui->sizeCombo->setCurrentIndex(int(fBookInfo.PageSize));
    ui->codecCombo->setCurrentIndex(int(fBookInfo.InternalCodec));
    ui->filterCombo->setCurrentIndex(int(fBookInfo.EnhanceLevel));

    if (fBookInfo.CoverPath.isEmpty() == false || fBookInfo.Cover.isNull() == false) {
        ui->coverChk->setChecked(true);
        if (fBookInfo.Cover.isNull()) {
            SetNewCover(fBookInfo.CoverPath);
        } else {
            ShowCover();
        }
    }
}

BookInfo TNewBookFrm::GetBookInfo()
{
    // Copy from fBookInfo
    BookInfo Result = fBookInfo;
    Result.Title = ui->titleEdt->text();
    Result.Author = ui->authorEdt->text();
    Result.Brief = ui->briefEdt->toPlainText();
    if (ui->coverChk->isChecked()) {
        Result.CoverPath = ui->coverEdt->text();
    } else {
        Result.CoverPath.clear();
        Result.Cover = QImage(); // leave fBookInfo's cover untouch?
    }
    Result.PageSize = ResizeMode(ui->sizeCombo->currentIndex());
    Result.InternalCodec = CodecMode(ui->codecCombo->currentIndex());
    Result.EnhanceLevel = FilterMode(ui->filterCombo->currentIndex());

    return Result;
}

void TNewBookFrm::onBrowseCoverClicked()
{
    QFileDialog OpenSaveFileDialog;
    QString coverpath = OpenSaveFileDialog.getOpenFileName(this, tr("Select Cover"), PathSetting.LastSourceFolder, "All Support Files (*.png;*.jpg;*.gif;*.bmp);;All Files (*.*)", 0, 0);
    if (coverpath.isEmpty()) {
        return;
    }
    ui->coverEdt->setText(coverpath);
    ui->coverChk->setChecked(true);
    PathSetting.LastSourceFolder = QFileInfo(coverpath).path();

    SetNewCover(coverpath);
}

void TNewBookFrm::SetNewCover( QString coverpath )
{
    fBookInfo.Cover = QImage(coverpath, MIMETypeToExt(GuessMIMEType(coverpath)).toAscii());
    ShowCover();
}

void TNewBookFrm::ShowCover()
{
    if (fBookInfo.Cover.isNull() == false) {
        TNekoDriver::ScaleCover(&fBookInfo.Cover);
        ui->coverView->setPixmap(QPixmap::fromImage(fBookInfo.Cover)); // Droped after use
    }
}