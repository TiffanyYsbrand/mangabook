#ifndef MAINUNT_H
#define MAINUNT_H

#include <QMainWindow>
#include <QFileDialog>
#include <QMimeData>
#include <QTreeWidgetItem>
#include "Common.h"
#include "OpenBook/OpenBook.h"
#include <QtGui/QLabel>
#include <QtGui/QProgressbar>



namespace Ui {
    class TMainFrm;
}

enum ResizeMode {
    rmSafe,
    rmPixelToPixel,
    rmFullScreen,
    rmIPad
};

enum CodecMode {
    cmPNG,
    cmJPEGVeryHigh,
    cmJPEGMedium
};

enum FilterMode {
    fmNone,
    fmSlight,
    fmHigh,
    fmMaximum
};

enum PageSortingMethod {
    psmAutomatic,
    psmAlpha,
    psmNone
};

struct BookInfo {
    QString Title;
    QString Author;
    QString Brief;
    QString CoverPath;
    QImage Cover;
    ResizeMode PageSize;
    CodecMode InternalCodec;
    FilterMode EnhanceLevel;
    BookInfo();
};

class TMainFrm : public QMainWindow {
    Q_OBJECT
public:
    explicit TMainFrm(QWidget *parent = 0);
    ~TMainFrm();

protected:
    void changeEvent(QEvent *e);

private:
    Ui::TMainFrm *ui;
    QFileDialog OpenSaveFileDialog, OpenSaveDBDialog, OpenSaveMpkDialog;
    QString fLastSavedProjectFilename;
    QLabel *action;
    QProgressBar *progress;
    QTime fPrepareBufferStartTime;
    bool fDiscardRenameCheck;
    QString fBookfilename;
    BookInfo fBookInfo;
    QTranslator* fTranslator;
    QString fLastLangCode;
    QString fCloudAppID;
private:
    void TryAcceptDrop( QFileInfo &info, int maxdepth, int &bypassoucnt, int &acceptcount );
    void TryAcceptPages( QFileInfo &info, int maxdepth, int chapterindex, int &bypasscount, int &acceptcount );
    void TryAcceptXmlFolder( QFileInfo &info, OpenBookWriter* writer, int baselen, int maxdepth, int &bypasscount, int &acceptcount );
public:
    void StoreTranslator( QTranslator* translator );
private slots:
    void onFileExitClicked();
    void onFileNewClicked();
    void onFileOpenClicked();
    void onFileCloseClicked();
    void onFileSaveClicked();
    void onFileSaveAsClicked();
    void onFolderToBookClicked();
    void onBookToFolderClicked();
    void onBuildBookClicked();
    void onChapterDropfiles(const QMimeData* data = 0);
    void onChapterWannaRenameValue(QTreeWidgetItem* item, int column);
    void onSortChapterClicked();
    void onMoveupChapterClicked();
    void onMovedownChapterClicked();
    void onAddChapterClicked();
    void onRemoveChapterClicked();
    void onClearChapterClicked();
    void onAddPageClicked();
    void onLanguageEnglishClicked();
    void onLanguageChineseClicked();
    void onToolsOptionsClicked();
    void onHelpContentsClicked();
public slots:
    void writeLog(QString content, TLogType logtype = ltMessage);
    void onReadyBuildBook();
};

QString CleanupTags(const QString& source);
QString LogTypeToString( TLogType logtype );
QString localLanguage();
#endif // MAINUNT_H
